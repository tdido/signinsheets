from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_CENTER
import datetime
import locale

from reportlab.lib.utils import ImageReader

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('startdate', type=datetime.date.fromisoformat,
                    help="ISO-formatted start date (e.g. 2025-10-31)")
parser.add_argument('enddate', type=datetime.date.fromisoformat,
                    help="ISO-formatted end date (e.g. 2025-11-30)")
args = parser.parse_args()

logo = ImageReader('img/logo_text.png')

pdfmetrics.registerFont(TTFont('VeraBd', 'VeraBd.ttf'))

locale.setlocale(locale.LC_ALL, 'es_ES.UTF8')

def dates_generator(start, end, delta):
    while (start <= end):
        yield start
        start += delta

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

styles = getSampleStyleSheet()

stylecell = ParagraphStyle(
    name='Normal',
    fontName='VeraBd',
    fontSize=11,
)

with open("data/students") as ifh:
    students = [l.rstrip("\n") for l in ifh]

students = [[Paragraph(s, stylecell), '', '', '', '', ''] for s in students]

with open("data/noclass") as ifh:
    noclass = [datetime.datetime.strptime(l.rstrip("\n"),'%Y%m%d').date() for l in ifh]

def create_sheet(students, week):
    mleft = 15
    aW = 590
    aH = 700

    firstday = str(week[0].day).zfill(2)
    firstdayn = week[0].strftime('%A')
    firstmonth = str(week[0].month).zfill(2)
    firstyear = week[0].year
    lastday = str(week[-3].day).zfill(2)
    lastdayn = week[-3].strftime('%A')
    lastmonth = str(week[-3].month).zfill(2)
    lastyear = week[-3].year

    canv = Canvas(f"output/{firstyear}{firstmonth}{firstday}_{lastyear}{lastmonth}{lastday}.pdf")

    canv.drawImage(logo, 90, 720, mask='auto')

    styletheader = ParagraphStyle(
        name='Normal',
        fontName='VeraBd',
        fontSize=13,
        textColor='white',
    )
    styletheader.alignment = TA_CENTER
    theader = [[Paragraph('Nombre',styletheader)] + [Paragraph(f"{d.strftime('%A')} {str(d.day).zfill(2)}/{str(d.month).zfill(2)}", styletheader) for d in week[:-2]]]

    data = theader + students
              
    t = Table(data, colWidths=[135] + [85] * 5, rowHeights=[40] + [30] * len(students))
    t.setStyle(TableStyle([("BOX", (0, 0), (-1, -1), 0.25, colors.black),
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BACKGROUND', (0, 0), (-1, 0), colors.gray),
                           ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                           ('BOTTOMPADDING', (0, 0), (-1, 0), 10),
   ]))

    data_len = len(data)

    for each in range(1,data_len):
        if each % 2 == 0:
            bg_color = colors.whitesmoke
        else:
            bg_color = colors.lightgrey

        t.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color)]))
        for i,d in enumerate(week):
            if d in noclass:
                t.setStyle(TableStyle([('BACKGROUND', (i+1, 0), (i+1, -1), colors.gray)]))

    header = Paragraph(f"""
            <bold><font size=18>Planilla de asistencia -
            {firstdayn} {firstday}/{firstmonth}/{firstyear} al {lastdayn}
            {lastday}/{lastmonth}/{lastyear}</font></bold>
    """, styles["BodyText"])

    w, h = header.wrap(aW, aH)
    header.drawOn(canv, mleft, aH)
    aH = aH - h - 10
    w, h = t.wrap(aW, aH)
    t.drawOn(canv, mleft, aH-h)

    canv.save()

delta = datetime.timedelta(days=1)

dates = [s for s in dates_generator(args.startdate, args.enddate, delta)]
weeks = chunks(dates, 7)
for week in weeks:
    create_sheet(students, week)
